<?php

namespace Drupal\image_transformation_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\Image;
use Drupal\Core\ImageToolkit\ImageToolkitManager;
use Drupal\Core\StreamWrapper\PublicStream;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ImageTransformationAPIController.
 */
class ImageTransformationAPIController extends ControllerBase {

  protected $imageFactory;
  protected $streamWrapperManager;
  protected $imageToolkitManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->imageFactory = $container->get('image.factory');
    $instance->streamWrapperManager = $container->get('stream_wrapper_manager');
    $instance->imageToolkitManager = $container->get('image.toolkit.manager');
    return $instance;
  }

  /**
   * Transform image given URL parameters.
   * 
   * @return Symfony\Component\HttpFoundation\Response
   *  Returns a Response which contains the transformed image in binary format.
   */
  public function transform(Request $request) {
    $image_uri = $this->decodeUrlSafeBase64($request->attributes->get('image_uri'));
    $stream_wrapper = $this->streamWrapperManager->getViaUri($image_uri);

    if (!$stream_wrapper || !($stream_wrapper instanceof PublicStream)) {
      return new JsonResponse([
        'error' => 'The image URI provided was not specified as a public URI. (eg. public://image.jpeg)'
      ], 400);
    }

    $input_transformations = $request->attributes->get('transformations') ?? '';
    $transformations = $this->parseTransformations($input_transformations);

    $dynamic_file_uri = $this->getDynamicDerivative($image_uri, $transformations);
    if ($dynamic_file_uri) {
      $image = $this->imageFactory->get($dynamic_file_uri);
      $uri = $image->getSource();
      
      // @see: ImageStyleDownloadController::deliver().
      $response = new BinaryFileResponse($uri, 200, [], FALSE);
      $response
        ->setPublic()
        ->setImmutable(TRUE)
        ->setExpires(new \DateTime('+1 year'))
        ->setMaxAge(365 * 24 * 60 * 60);
    }
    else {
      $response = new JsonResponse([
        'error' => 'The requested image can not be generated.'
      ], 500);
    }

    return $response;
  }

  /**
   * Parses a serialized representation of transformations.
   *
   * @param string $serialized_transformations
   *   The serialized transformations; e.g.
   *   c_crop,x_0,y_0,width_100,height_100. The c_OPERATION must be the first
   *   element in the comma-delimited list.
   *
   * @return array
   *   The ordered list of operations, each entry an array with:
   *   - operation: the operation to perform, e.g. crop, resize_and_crop.
   *   - parameters: the operation parameters, as supported by each operation.
   */
  private function parseTransformations($serialized_transformations) {
    $parsed = [];

    $transformations = explode('|', $serialized_transformations);
    foreach ($transformations as &$transformation) {
      $transform_components = explode(',', $transformation);
      // we assume the first component: (c_crop),x_1,y_1 is the operation
      list(, $operation) = explode('_', $transform_components[0]);
      $operation_parameters = [];

      for ($i = 1; $i < count($transform_components); $i++) {
        list($parameter_key, $parameter_value) = explode('_', $transform_components[$i]);
        $operation_parameters[$parameter_key] = $parameter_value;
      }
      ksort($operation_parameters);

      $parsed[] = [
        'operation' => $operation,
        'parameters' => $operation_parameters,
      ];
    }

    return $parsed;
  }

  /**
   * @todo docblock.
   */
  private function getDynamicDerivative($source, array $transformations) {
    $stream_wrapper = $this->streamWrapperManager->getViaUri($source);
    $hash = hash('md5', $source . '::' . serialize($transformations));
    $pathinfo = pathinfo($stream_wrapper->getUri());
    $dynamic_file_uri = 'public://styles-dynamic/' . $hash . '/' . $pathinfo['filename'] . '.' . $pathinfo['extension'];

    if (!file_exists($dynamic_file_uri)) {
      $this->createDynamicDerivative($source, $dynamic_file_uri, $transformations);
    }

    if (file_exists($dynamic_file_uri)) {
      return $dynamic_file_uri;
    }

    return FALSE;
  }

  /**
   * @todo docblock.
   */
  private function createDynamicDerivative($source, $destination, array $transformations) {
    $image = $this->imageFactory->get($source);
    if (!$image->isValid()) {
      return FALSE;
    }

    // Get the folder for the final location of this style.
    $directory = \Drupal::service('file_system')->dirname($destination);

    // Build the destination folder tree if it doesn't already exist.
    if (!\Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      \Drupal::logger('image_transformation_api')->error('Failed to create dynamic style directory: %directory', ['%directory' => $directory]);
      return FALSE;
    }

    // Apply transforms
    foreach ($transformations as &$transformation) {
      $image->apply($transformation['operation'], $transformation['parameters']);
    }

    if (!$image->save($destination)) {
      if (file_exists($destination)) {
        \Drupal::logger('image_transformation_api')->error('Cached image file %destination already exists. There may be an issue with your rewrite configuration.', ['%destination' => $destination]);
      }
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Translate a URL-safe base64 to regular base64 before decoding.
   *   + to -
   *   / to _
   *   = to .
   */
  private function decodeUrlSafeBase64($url_safe_base_64) {
    return urldecode(base64_decode(strtr($url_safe_base_64, '-_.', '+/=')));
  }

}
